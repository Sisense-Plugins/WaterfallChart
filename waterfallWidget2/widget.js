
/*
	Todo List:

		1. Handle errors, while building the widget panels
		2. Add drilldown option
		3. Null value handling

*/

var panels = {
	"dim": "Categories",
	"metric": "Metric",
	"date": "Time Field"
}

var colors = {
	"up": "#00a808",
	"down": "#cd202c"
}

prism.registerWidget("waterfall2", {

	name : "waterfall2",
	family : "Column",
	title : "Waterfall v2",
	hideNoResults: true,
	iconSmall : "/plugins/waterfallWidget2/widget-24.png",
	styleEditorTemplate: "/plugins/waterfallWidget2/styler.html",
	style: {
		offset: 1,
		offsetLevel: "years",
		fontColor: "black",
		inside: true,
		diff: "variance",
		yAxisScale: "linear"
	},
	data : {
		selection : [],
		defaultQueryResult : {},	
		panels : [
			{
				name: panels.dim,
				type: 'visible',
				//itemAttributes: ["color"],
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: panels.metric,
				type: 'visible',
				itemAttributes: ["color"],
				allowedColoringTypes: function() {
					return {
						color: true,
						condition: false,
						range: false
					}
				},
				metadata: {
					types: ['measures'],
					maxitems: 1
				},
				itemAdded: function(widget, item) {
					var colorFormatType = $$get(item, "format.color.type");
					if ("color" === colorFormatType || "color" === colorFormatType) {
						var color = item.format.color.color;
						defined(color) && "transparent" != color && "white" != color && "#fff" != color && "#ffffff" != color || $jaql.resetColor(item)
					}
					"range" === colorFormatType && $jaql.resetColor(item), defined(item, "format.color_bkp") && $jaql.resetColor(item), defined(item, "format.members") && delete item.format.members
				}
			},
			{
				name: panels.date,
				type: 'visible',
				//itemAttributes: ["color"],
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				}
			},
			{
				name: 'filters',
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],
		
		canColor: function (widget, panel, item) {

            return (panel.name === panel.metric );
        },
		
		allocatePanel: function (widget, metadataItem) {
			
			// measure
			if (prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel(panel.metric).items.length === 0) {

				return panel.metric;
			}
			// dimension
			else if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel(panel.dim).items.length === 0) {

				return panel.dim;
			}
		},

		// returns true/ reason why the given item configuration is not/supported by the widget
		isSupported: function (items) {

			return this.rankMetadata(items, null, null) > -1;
		},

		// ranks the compatibility of the given metadata items with the widget
		rankMetadata: function (items, type, subtype) {

			var a = prism.$jaql.analyze(items);

			// require 1 measure, 2 dimension
			if (a.measures.length == 1 && a.dimensions.length == 2) {

				return 0;
			}

			return -1;
		},

		// populates the metadata items to the widget
		populateMetadata: function (widget, items) {

			var a = prism.$jaql.analyze(items);

			// allocating dimensions
			if (a.dimensions.length > 0) {
				widget.metadata.panel(panels.dim).push(a.dimensions[0]);
			}
			if (a.measures.length > 0) {
				widget.metadata.panel(panels.metric).push(a.measures[0]);
			}
			if (a.dimensions.length > 1) {
				widget.metadata.panel(panels.date).push(a.dimensions[1]);
			}

			// allocating filters
			widget.metadata.panel("filters").push(a.filters);
		},

		// builds a jaql query from the given widget
		buildQuery: function (widget) {

			//	Reset the query complete counter
			widget.options.query1 = null;
			widget.options.query2 = null;
			widget.queryResult = {};
			
			var queryTemplate = { datasource: widget.datasource, metadata: [] }

			/****************************************/
			/*	Define common jaql items 			*/
			/****************************************/

			//	Get the end metric
			var endMetric = $$.object.clone(widget.metadata.panel(panels.metric).items[0],true),
				dateDim = widget.metadata.panel(panels.date).items[0],
				dimension = widget.metadata.panel(panels.dim).items[0];
			if (endMetric.field){
				delete endMetric.field;
			}

			//	Define guids and context for the start metric
			var startMetricValueGuid = getGuid(),
				startMetricValueContext = $$.object.clone(endMetric.jaql, true),
				startMetricTimeGuid = getGuid(),
				startMetricTimeContext = $$.object.clone(dateDim.jaql, true),
				timeOffset = widget.style.offset;

			//	Update the styler's label
			widget.style.offsetLevel = dateDim.jaql.level;

			//	Define the formatting for the start metric
			var startMetricFormat = endMetric.format ? $$.object.clone(endMetric.format, true) : {};

			var queryFormula = '( ' + startMetricValueGuid + ' , prev(' + startMetricTimeGuid + ',' + timeOffset + ') )';
			//var queryFormula = queryFunction + '( ' + startMetricValueGuid + ' )';
		
			// Get the metric to use
			var startMetric = {
				'format': startMetricFormat,
				'jaql': {
					'context': {},
					'formula': queryFormula,
					'type': 'measure',
					'title': 'Start Value'
				}
			};

			//	Assign Context
			startMetric.jaql.context[startMetricValueGuid] = startMetricValueContext;
			startMetric.jaql.context[startMetricTimeGuid] = startMetricTimeContext;

			//	Get the list of widget and dashboard filters
			var filters = getFilters(widget);

			/*
			//	Build the pastX function
			var queryFunction = '';
			if (dateDim.jaql.level == "years") {
				queryFunction = "PastYear";
			} else if (dateDim.jaql.level == "quarters") {
				queryFunction = "PastQuarter";
			} else if (dateDim.jaql.level == "months") {
				queryFunction = "PastMonth";
			} else if (dateDim.jaql.level == "weeks") {
				queryFunction = "PastWeek";
			} else if (dateDim.jaql.level == "days") {
				queryFunction = "PastDay"
			}
			*/

			/****************************************/
			/*	Build the YoY query 				*/
			/****************************************/
	    
			var query1 = $$.object.clone(queryTemplate,true);		

			//	Add to the query
			query1.metadata.push(startMetric);
			query1.metadata.push(endMetric);

			// pushing filters
			filters.forEach(function (item) {
				item = $$.object.clone(item, true);
				item.panel = "scope";
				query1.metadata.push(item);
			});

			//  Get the http service
            var $http = prism.$injector.get('$http');

            //	Get the jaql endpoint
            var url = widget.$query.createJaqlRequestUrl(widget.datasource);

            //	Save the promise to the API call
            $http.post(url, angular.toJson(query1))
            	.then(function(response){
            		//	Safely check for the start and end value
            		var values = $$get(response, 'data.values',[null,null]),
            		 	startValue = values[0] ? values[0].data : 0,
            			endValue = values[1] ? values[1].data : 0;

            		//	Save the intial query's response
            		widget.options.query1 = {
            			'start': startValue,
            			'end': endValue
            		};

            		//	Trigger next event	
            		//console.log("Waterfall Widget 2: query 1 complete");
            		customRender(widget);
            	})

			/****************************************/
			/*	Build the dimension split query 	*/
			/****************************************/

			var query2 = $$.object.clone(queryTemplate,true);

			/*

			//	Define guids and context
			var startMetricValueGuid2 = getGuid(),
				startMetricValueContext2 = $$.object.clone(endMetric.jaql, true),
				startMetricTimeGuid2 = getGuid(),
				startMetricTimeContext2 = $$.object.clone(dateDim.jaql, true),
				timeOffset2 = widget.style.offset;

			//	Define the formatting for the start metric
			var startMetricFormat2 = endMetric.format ? $$.object.clone(endMetric.format, true) : {};

			//	Define the formula to use
			var myFormula = startMetricValueGuid2 + ' - ( ' + startMetricValueGuid2 + ' , prev(' + startMetricTimeGuid2 + ',' + timeOffset2 + ') )';
			
			// Get the metric to use
			var varianceMetric = {
				'format': startMetricFormat2,
				'jaql': {
					'context': {},
					'formula': myFormula,
					'type': 'measure',
					'title': 'Variance'
				}
			};

			//	Assign Context
			varianceMetric.jaql.context[startMetricValueGuid2] = startMetricValueContext;
			varianceMetric.jaql.context[startMetricTimeGuid2] = startMetricTimeContext;

			*/

			//	Build the query metadata
			query2.metadata.push(dimension);
			query2.metadata.push(startMetric);
			query2.metadata.push(endMetric);

			// pushing filters
			filters.forEach(function (item) {
				item = $$.object.clone(item, true);
				item.panel = "scope";
				query2.metadata.push(item);
			});

			//	Save the promise to the API call
            $http.post(url, angular.toJson(query2))
            	.then(function(response){
            		
            		//	Safely check for the values
            		var values = $$get(response, 'data.values',[]);

            		//	Save the intial query's response
            		widget.options.query2 = {
            			data: values
            		};

            		//	Trigger next event	
            		//console.log("Waterfall Widget 2: query 2 complete");
            		customRender(widget);
            	})

            //	Pass back an empty query
            return queryTemplate;
		},

		//create highcharts data structure
		processResult : function (widget, queryResult) {
			return queryResult;
		}
	},

	render : function (widget, args) {
		//	Only run if both queries are complete
		if (widget.options.query1 && widget.options.query2){
			customRender(widget);
		}
	},

	destroy : function (widget, args) {}
});

/********************************/
/* 	Override Functions 			*/
/********************************/

//	Function to render the widget
function customRender(widget,args){
	
	//	Only run if all the data has been returned
	var queriesComplete = (widget.options.query1 && widget.options.query2);
	if (queriesComplete){
		//console.log('Waterfall Widget 2: All queries finished, OK to render');

		//	Process the results of the widget
		widget.queryResult = customProcessResults(widget, widget.options.query1, widget.options.query2);

		// 	Get widget elements
		var widgetEl = (prism.$ngscope.appstate == "widget") ? $('.widget-body') : $('.widget-body', $('widget[widgetid="' + widget.oid + '"]'));
		widgetEl.empty();		
		
		//	Calculate the widget and height of the container
		var width = widgetEl.width() - 8,
			height = widgetEl.height() - 8;
		
		// Prepare the container div
		var divId = "waterfall2-" + widget.oid;
		widgetEl.attr('id', divId);
		widgetEl.css('width', "99%");
		widgetEl.css('height', "99%");
		widgetEl.css('margin', "0 auto");

		//	Render the chart
		$('#'+divId).highcharts(widget.queryResult)
	}
}

//	Create a highcharts config object 
function customProcessResults(widget, query1, query2){

	/********************************/
	/* 	Utility Functions 			*/
	/********************************/

	/*	Figure out the metadata 	*/
	var dimMetadata = widget.metadata.panel(panels.dim).items[0].jaql,
		dateMetadata = widget.metadata.panel(panels.date).items[0].jaql,
		metricMetadata = widget.metadata.panel(panels.metric).items[0].jaql;

	//	Get the formatting mask for the metric
	var metricMask = $$get(widget.metadata.panel(panels.metric).items[0], 'format.mask', null);

	//	Get the sisense number formatter
	var formatter = prism.$injector.get('$filter')('numeric');

	//	Figure out the labels for start and end columns (based on date filter and level)
	function getStartEnd(widget){

		//	Get the widget's date dim
		var dateDim = widget.metadata.panel(panels.date).items[0].jaql;

		//	Get the list of widget and dashboard filters
		var filters = getFilters(widget);

		//	Find any filters that match the dim + level of the date dim
		var matches = [];
		function checker(filter){
			var isMatch = (!filter.disabled) && (filter.jaql.dim == dateDim.dim);
			if (isMatch){
				matches.push(filter);
			}
		}
		
		//	Check filters
		filters.forEach(checker);

		//	What if no matches?
		if (matches.length == 0){
			
			//	No matches
			return {
				"start": "Start",
				"end": "End"
			}

		} else {

			//	Found a match

			return {
				"start": "Start",
				"end": "End"
			}
		}
	}

	//	Value Label handler
	function customValueLabel(){
		//	Format the value
		var valueRaw = this.point.yValue ? this.point.yValue : this.y;
		var valueLabel = formatter(valueRaw, metricMask);
		return valueLabel;
	}

	//	Value Label handler
	function customAxisLabel(){
		//	Format the value
		var valueLabel = formatter(this.value, metricMask);
		return valueLabel;
	}

	//	Tooltip handler
	function customTooltip(){
		//	Format the value
		var valueRaw = this.point.yValue ? this.point.yValue : this.y;
		var valueLabel = formatter(valueRaw, metricMask);
		//	Return an HTML tooltip
		return '<b>' + this.key + '</b>: ' + valueLabel;
	}

	/********************************/
	/* 	Business Logic  			*/
	/********************************/

	/* 	Figure out the labels for Start and End 	*/
	var labels = getStartEnd(widget);
	colors.ends = prism.$ngscope.dashboard.style.options.palette.colors[0];

	/* 	Create the data array 		*/
	var data = [];

	//	Define the starting point
	var start = {
		'name': labels.start,
		'color': colors.ends,
		'y': query1.start
	}
	data.push(start);

	//	Loop through each dimension
	query2.data.forEach(function(item){

		//	Calculate the differene between time periods
		var value = 0;
		if (widget.style.diff == "variance"){
			value = item[2].data - item[1].data;
		} else if (widget.style.diff == "growth") {
			value = (item[2].data - item[1].data) /  item[1].data;
		}
		
		//	Create a new data point 
		var point = {
			'name': item[0].text,
			'color': value >=0 ? colors.up : colors.down,
			'y': value
		}

		//	Save to array
		data.push(point);
	})

	//	Define the ending point
	var end = {
		'name': labels.end,
		'color': colors.ends,
		'isSum': true,
		'y': query1.end,
		'yValue': query1.end
	}
	data.push(end);

	/*	Formatting Style Options 	*/
	var xAxisLabel = widget.style.xAxisTitle ? dimMetadata.title : '',
		dataLabelPlacement = widget.style.inside;

	/*	Highcharts config 		 	*/
	var config = {
		chart: {
			type: 'waterfall'
		},
		title: {
			text: ''
		},
		xAxis: {
			type: 'category',
			title: {
				text: xAxisLabel
			}
		},
		yAxis: {
			title: {
				text: '',
				rotation: 0
			},
			labels: {
				formatter: customAxisLabel
			},
			type: widget.style.yAxisScale
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: customTooltip
		},
		series: [{
			upColor: Highcharts.getOptions().colors[2],
			color: Highcharts.getOptions().colors[3],
			data: data,
			dataLabels: {
				enabled: true,
				formatter: customValueLabel,
				inside: dataLabelPlacement,
				style: {
					color: widget.style.fontColor,
					fontWeight: 'bold'
				}
			},
			pointPadding: 0
		}]
	};

	return config;
}

//	Function to get a complete list of all filters to include in jaql query
function getFilters(widget){

	//	Get widget & dashboard level filters
	var filters = widget.metadata.panel('filters').items.filter(function(f){ return !f.disabled}),
	 	dashboardFilters = widget.dashboard.filters.$$items.filter(function(f){ return !f.disabled}),
	 	ignoreList = widget.metadata.ignore.dimensions;

	//	Get a list of all enabled filters, but taking into account that widget filters take precedence over dashboard filters
	dashboardFilters.forEach(function(df){

		//	Handle cascading filters, by flattening into an array
		var dfa = [];
		if (df.isCascading){

			//	Dependant filter, flatten into array
			df.levels.forEach(function(level){

				//	Check the widget ignore list
				if (ignoreList.indexOf(level.dim)<0) {
					dfa.push({ jaql: level });
				}
			})
		} else {

			//	Regular filter, just an array of one
			if (ignoreList.indexOf(df.jaql.dim)<0) {
				//	Check the widget ignore list
				dfa.push(df);
			}
		}

		//	Loop through this dashboard filter's array
		dfa.forEach(function(sdf){

			//	Has the filter already been added?
			var existing = filters.filter(function(wf){
				if (sdf.jaql.level){
					return ((sdf.jaql.level == wf.jaql.level) && (sdf.jaql.dim == wf.jaql.dim));
				} else {
					return (sdf.jaql.dim == wf.jaql.dim);
				}
			})
			
			//	No matching filters at the widget level, so ok to add
			if (existing.length==0){
				filters.push(sdf);
			}
		})
	})

	//	Return a flat list of filters, combining widget and dashboard filter panels
	return filters;

}

//	Function to create a guid for jaql contexts
function getGuid(){
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
		  .toString(16)
		  .substring(1);
	}
	return '[' + s4() + '-' + s4() + ']'
}