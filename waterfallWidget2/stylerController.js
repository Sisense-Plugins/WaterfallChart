
mod.controller('stylerController', ['$scope',
    function ($scope) {

        /**
         * variables
         */


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            $scope.model = $$get($scope, 'widget.style');
        });



        /**
         * public methods
         */

         $scope.changeDiff = function (value) {

            $scope.model.diff = value;

            _.defer(function () {
                $scope.$root.widget.refresh();

            });
        };

        $scope.changeScale = function (value) {

            $scope.model.yAxisScale = value;
            debugger

            _.defer(function () {
                $scope.$root.widget.refresh();

            });
        };

        $scope.changeFontColor = function (fontColor) {

        	$scope.model.fontColor = fontColor;

        	_.defer(function () {
        		$scope.$root.widget.redraw();

        	});
        };
		
		$scope.changeLabelLocation = function (inside) {

        	$scope.model.inside = inside;
			//automatically set to black, since the background is white
			$scope.model.fontColor = 'black';

        	_.defer(function () {
        		$scope.$root.widget.redraw();

        	});
        };

        $scope.refreshWidget = function () {         
            
            //  Redraw the widget
            _.defer(function () {
                $scope.$root.widget.refresh();
            });
        };  
    }
]);